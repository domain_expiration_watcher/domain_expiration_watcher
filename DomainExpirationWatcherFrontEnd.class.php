<?php
/*
 * This file is part of Domain Expiration Watcher
 *
 * Copyright (C) 2006 Simó Albert i Beltran
 * 
 * Domain Expiration Watcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Domain Expiration Watcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Domain Expiration Watcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 * http://www.gnu.org/licenses/agpl.txt
 *
 * Version 0.0.1
 */

class DomainExpirationWatcherFrontEnd
{
	private $database_type = "dewFileDataBase";
	private $notificator_type = "dewEmailNotificator";
	private $database;
	private $notificator;
	private $email;
	private $domain;
	private $hash;
	
	public function __construct()
	{
		require_once($this->database_type . ".class.php");
		$this->database = new $this->database_type;
		require_once($this->notificator_type . ".class.php");
		$this->notificator = new $this->notificator_type;
	}

	public static function checkEmail($email)
	{
		if (!empty($email) && preg_match("/^[0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\.[a-z]{2,4}$/", $email))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static function checkDomain($domain)
	{
		if(!empty($domain) && preg_match("/^[0-9a-z]([-_]?[0-9a-z])*\.[a-z]{2,4}$/", $domain))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	private function subscribeEmailDomain()
	{
		if(!$this->database->isSubscribedEmailDomain($this->email, $this->domain))
		{
			require_once("DomainExpirationWatcher.class.php");
			$domain_expiration_watcher = new DomainExpirationWatcher;
			$next_notification_time = $domain_expiration_watcher->getNextNotificationTime($this->domain);
			if($this->database->subscribeEmailDomain($this->email, $this->domain, $next_notification_time))
			{
				$this->notificator->sendEmailNotificationSubscribeEmailDomain($this->email,$this->domain);
				return "Subscribed " . $this->email . " to " . $this->domain;
			}
			else
			{
				return "Sorry try later or contact admin.";
			}

		}
		else
		{
			return "You are subscribed.";
		}
	}

	private function unsubscribeEmailDomain()
	{
		if($this->database->isSubscribedEmailDomain($this->email, $this->domain))
		{
			if($this->database->unsubscribeEmailDomain($this->email, $this->domain))
			{
				$this->notificator->sendEmailNotificationUnsubscribeEmailDomain($this->email, $this->domain);
				return "Unsubscribed " . $this->email . " to " . $this->domain;
			}
		}
		return "You are unsubscribed.";
	}

	private function action($action)
	{
		switch($action){
			case 'subscribe':
				return $this->subscribeEmailDomain();
			break;
			case 'unsubscribe':
				return $this->unsubscribeEmailDomain();
			break;
		}
	}

	public function frontEnd()
	{
		if($_GET['action'] != 'subscribe' && $_GET['action'] != 'unsubscribe' && $_GET['action'] != 'show')
		{
			$message = "Select an action, please.";
		}
		else
		{
			$this->domain = trim($_GET['domain']);
			if(!$this->checkDomain($this->domain))
			{
				$message = "Sorry, not valid domain, try again.";
			}
			else
			{
				if($_GET['action'] == 'show')
				{
					require_once("DomainExpirationWatcher.class.php");
					$domain_expiration_watcher = new DomainExpirationWatcher();
					$expiration_time = $domain_expiration_watcher->getDomainExpirationTime($this->domain);
					$expiration_date = date('c', $expiration_time);
					$expiration_left = $domain_expiration_watcher->getDaysToDomainExpiration($this->domain);

					$message = "Domain " . $this->domain . " expire on " . $expiration_left . " days.";
					$message .= " Expiration date: " .  $expiration_date . "\n";
				}
				else
				{
					if(isset($_GET['email']))
					{
						if(!$this->checkEmail($_GET['email']))
						{
							$message = "Incorrect e-mail address.";
						}
						else
						{
							$this->email = trim($_GET['email']);
							$this->hash = md5(rand());

							$create_confirmation = $this->database->createConfirmation($this->hash, $_GET['action'], $this->email, $this->domain);
							$send_confirmation = $this->notificator->sendEmailConfirmationEmailDomain($this->email, $this->domain, $_GET['action'], $this->hash);

							if($create_confirmation && $send_confirmation)
							{
								$message = "Can you confirm " . $_GET['action'] . " to " . $_GET['domain'] . " expiration notifications by clicking on the link received by e-mail.";
							}
							else
							{
								$message = "Sorry, try later or contact admin.";
							}
						}
					}
				}
			}
		}
		if(isset($_GET['confirmation']))
		{
			if($this->database->confirmationExists($_GET['confirmation']))
			{
				$action = $this->database->getConfirmationAction($_GET['confirmation']);
				$this->email = $this->database->getConfirmationEmail($_GET['confirmation']);
				$this->domain = $this->database->getConfirmationDomain($_GET['confirmation']);
				$message = $this->action($action);
				$this->database->removeConfirmation($_GET['confirmation']);
			}
			else
			{
				$message = "Sorry, wrong confirmation key, please fill the form again.";
			}

		}
		include('form.php');
	}
}
?>



