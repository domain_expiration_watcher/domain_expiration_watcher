<?php
/*
 * This file is part of Domain Expiration Watcher
 *
 * Copyright (C) 2006 Simó Albert i Beltran
 * 
 * Domain Expiration Watcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Domain Expiration Watcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Domain Expiration Watcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 * http://www.gnu.org/licenses/agpl.txt
 *
 * Version 0.0.1
 */

class dewFileDataBase
{
	private $data_dir = 'data';
	private $data_directory_permissions = 0755;
	private $data_file_permissions = 0666;

	public function confirmationExists($confirmation)
	{
		return file_exists($this->data_dir . "/confirmation/" . $confirmation);
	}

	public function getConfirmationAction($confirmation)
	{
		if($this->confirmationExists($confirmation))
		{
			$data = file($this->data_dir . "/confirmation/" . $confirmation);
			return trim($data[0]);
		}
	}
	
	public function getConfirmationEmail($confirmation)
	{
		if($this->confirmationExists($confirmation))
		{
			$data = file($this->data_dir . "/confirmation/" . $confirmation);
			return trim($data[1]);
		}
	}
	
	public function getConfirmationDomain($confirmation)
	{
		if($this->confirmationExists($confirmation))
		{
			$data = file($this->data_dir . "/confirmation/" . $confirmation);
			return trim($data[2]);
		}
	}

	public function createConfirmation($confirmation, $action, $email, $domain)
	{
		$confirmation_data = $action . PHP_EOL . $email . PHP_EOL . $domain;
		if(!$this->confirmationExists($confirmation))
		{
			if(file_put_contents($this->data_dir . "/confirmation/" . $confirmation, $confirmation_data))
			{
				return true;
			}
		}
		return false;
	}

	public function removeConfirmation($confirmation)
	{
		if($this->confirmationExists($confirmation))
		{
			unlink($this->data_dir . "/confirmation/" . $confirmation);
		}
	}

	public function isSubscribedEmailDomain($email, $domain)
	{
		if(file_exists($this->data_dir . "/email/" . $email . "/" . $domain))
		{
			return true; 
		}
		else
		{
			return false;
		}
	}

	public function subscribeEmailDomain($email, $domain, $next_notification_time)
	{
		$storage_dir = $this->data_dir . "/email/";
		@mkdir($storage_dir . $email, $this->data_directory_permissions, true);
		if(@file_put_contents($storage_dir . $email . "/" . $domain, $next_notification_time))
		{
			chmod($storage_dir . $email . "/" . $domain, $this->data_file_permissions);
			return true;
		}
		else
		{
			return false;
		}
	}

	public function unsubscribeEmailDomain($email, $domain)
	{
		$storage_dir = $this->data_dir . "/email/";
		if(@unlink($storage_dir . $email . "/" . $domain))
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	public function getSavedDomainExpirationDate($domain)
	{
		$file = $this->data_dir . "/domain/" . $domain;
		if (file_exists($file))
		{
			return file_get_contents($this->data_dir . "/domain/" . $domain);
		}
		else
		{
			return false;
		}
	}

	public function	saveDomainExpirationDate($domain,$expiration_date)
	{
		$storage_dir = $this->data_dir . "/domain/";
		if(!file_exists($storage_dir)){
			mkdir($storage_dir, $this->data_directory_permissions, true);
		}
		return file_put_contents($storage_dir . $domain, $expiration_date);
	}

	public function setNextNotificationEmailDomain($email, $domain, $time)
	{
		return file_put_contents($this->data_dir . "/email/" . $email . "/" . $domain, $time);
	}

	public function getNextNotificationEmailDomain($email, $domain)
	{
		return file_get_contents($this->data_dir . "/email/" . $email . "/" . $domain);
	}

	public function getEmails()
	{
		$storage_dir = $this->data_dir . "/email/";
		if(file_exists($storage_dir))
		{
			$emails = scandir($storage_dir);
			// Skip "." and ".." relative directories. 
			if ($emails[0] == ".") unset($emails[0]);
			if ($emails[1] == "..") unset($emails[1]);
			return $emails;
		}
	}

	public function getEmailDomains($email)
	{
		$storage_dir = $this->data_dir . "/email/" . $email;
		if(file_exists($storage_dir))
		{
			// Get domains for user
			$domains = scandir($storage_dir);
			// Skip "." and ".." relative directories. 
			if ($domains[0] == ".") unset($domains[0]);
			if ($domains[1] == "..") unset($domains[1]);
			return $domains;
		}
	}
}
?>
